# Archivoz

_Archivoz_ es un proyecto de [Intercambios Transorgánicos](https://intercambiostransorganicos.org/), equipo de diseño y desarrollo de interfaces.
El interés principal de este proyecto es reinterpretar y colaborar con comprender problemáticas sociales, y en particular, del campo de la salud vocal.
La App _“Archivoz”_ es un archivo de voces dinámico que funciona como un _sistema de visualización de datos y como una aplicación “text to speech”._

Basado en una implementacion de https://github.com/mathigatti/tts-web 

![Arquitectura Back + Front](imgs/_IT__Archivoz__arquitectura_de_solución.jpg)


## Repositorio para el back-end del proyecto
### Requerimientos
- NumPy >= 1.11.1
- TensorFlow >= 1.3 (Note that the API of tf.contrib.layers.layer_norm has changed since 1.3)
- librosa
- tqdm
- matplotlib
- scipy

## Instalacion

```bash
#Instalacion manual
docker build . -t tts
docker run -p 8080:8080 --memory="2g" --cpus="1" tts
docker tag gpt2 gcr.io/[PROJECT-ID]/tts
docker push gcr.io/[PROJECT-ID]/tts

#Instalacion por Gitlab ci
docker run registry.gitlab.com/intercambiostransorganicos1/archivoz/archivoz:latest -p 80

```

## Uso 
- Cargar los modelos en la database y añadir nombres al diccionario 
- Pasar texto a traves del parametro text y modelo a traves del parametro model

```bash 
lang = {"santi-min":"es", "max-min":"es", "cami-min":"es", "spinetta":"es"} 
text = params.get('text', 'Hola soy fulanite') 
model = params.get('model', 'santi-min
```

## Contribuciones
Pull requests son bienvenidas. Para cambios mayores, por favor abrir un issue para evaluarlos.
