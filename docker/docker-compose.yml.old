version: '3'
services:
  backend:
    depends_on:
      - minio
    image: registry.gitlab.com/intercambiostransorganicos1/tts-web1/ttsweb:latest
    container_name: ttsweb
    volumes:
      - ./minio/data/models:/models
    labels:
      - "traefik.backend=backend"
      - "traefik.frontend.rule=Host:backend.archivoz.ai"
      - "traefik.frontend.entryPoints=http,https"
      - "traefik.port=8080"
      - "traefik.enable=true"
    restart: always

  minio:
    image: minio/minio:latest
    container_name: minio
    volumes:
      - ./minio/data:/data
    environment:
      MINIO_ACCESS_KEY: ${MINIO_KEY}
      MINIO_SECRET_KEY: ${MINIO_SECRET}
    labels:
      - "traefik.backend=minio"
      - "traefik.frontend.rule=Host:minio.archivoz.ai"
      - "traefik.frontend.entryPoints=http,https"
      - "traefik.port=9001"
      - "traefik.enable=true"
    command: server /data --console-address ":9001"

  traefik:
    image: traefik:1.7-alpine
    depends_on:
      - backend
    ports:
      - "80:80"
      - "443:443"
    labels:
      - "traefik.backend=traefik"
      - "traefik.frontend.rule=Host:traefik.archivoz.ai"
      - "traefik.frontend.entryPoints=http,https"
      - "traefik.port=8080"
      - "traefik.enable=true"
    restart: always
    volumes:
      - ./etc/traefik.toml:/etc/traefik/traefik.toml
      - /var/run/docker.sock:/var/run/docker.sock
      - ./etc/acme.json:/acme.json

